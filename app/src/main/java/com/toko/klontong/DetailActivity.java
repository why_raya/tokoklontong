package com.toko.klontong;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.toko.klontong.utils.model.Model;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.Utils.accountValid;
import static com.toko.klontong.utils.Utils.getCurrency;

public class DetailActivity extends AppCompatActivity {

    private Model m;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        ImageView img  = findViewById(R.id.product_image);
        TextView title = findViewById(R.id.product_title);
        TextView price = findViewById(R.id.product_price);
        TextView desc  = findViewById(R.id.product_desc);
        TextView discount = findViewById(R.id.product_discount);
        TextView discountPrice = findViewById(R.id.product_price_discount);

        m = app().model;
        try {
            Glide.with(this).load(m.getImage()).into(img);
            title.setText(m.getName());
            price.setText(getCurrency(m.getPrice()));
            desc.setText(Html.fromHtml(m.getDesc()));
            discount.setVisibility(m.getDiscount() > 0 ? View.VISIBLE : View.GONE);
            discountPrice.setVisibility(m.getDiscount() > 0 ? View.VISIBLE : View.GONE);

            if (m.getDiscount() > 0) {
                discount.setText(getResources().getString(
                        R.string.title_discount) + " " + m.getDiscount() + "%");
                discountPrice.setText(getCurrency(m.getFixedPrice()));
                price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else
                price.setPaintFlags(price.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        } catch (Exception e) {
            e.printStackTrace();
        }

        findViewById(R.id.add_to_cart).setOnClickListener(v -> {
            app().cartData.add(m);
            onBackPressed();
        });

        if (check()) {
            showInfo(getString(R.string.product_exist_in_cart));
        } else {
            if (!accountValid())
                showInfo(getString(R.string.info_need_login));
            findViewById(R.id.add_to_cart).setVisibility(accountValid() ? View.VISIBLE : View.GONE);
        }

    }

    private void showInfo(String info) {
        Snackbar snackBar = Snackbar.make(findViewById(android.R.id.content), info, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", v -> snackBar.dismiss());
        snackBar.show();
    }

    private boolean check() {
        for (Model model : app().cartData) {
            if (model.getId() == this.m.getId())
                return true;
        }
        return false;

    }
}
