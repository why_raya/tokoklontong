package com.toko.klontong;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.material.snackbar.Snackbar;
import com.toko.klontong.adapter.ProductAdapter;
import com.toko.klontong.fragment.Cart;
import com.toko.klontong.fragment.Map;
import com.toko.klontong.fragment.Order;
import com.toko.klontong.fragment.OrderDetail;
import com.toko.klontong.fragment.User;
import com.toko.klontong.utils.GridDecoration;
import com.toko.klontong.utils.MyNetwork;
import com.toko.klontong.utils.PermissionHelper;
import com.toko.klontong.utils.PreferencesHelper;
import com.toko.klontong.utils.RecyclerItemClickListener;
import com.toko.klontong.utils.model.Model;

import java.util.ArrayList;
import java.util.Objects;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.MyNetwork.getStatus;
import static com.toko.klontong.utils.Utils.accountValid;
import static com.toko.klontong.utils.Utils.dpToPx;
import static com.toko.klontong.utils.Utils.getEmail;
import static com.toko.klontong.utils.Utils.setOnClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        RecyclerItemClickListener.OnItemClickListener{

    private enum Tag {
        HOME, CART, USER, MAP, ORDER, ORDER_DETAIL
    }

    private Handler mHandler = new Handler();
    private Tag mTag = Tag.HOME;
    private ArrayList<Model> data = new ArrayList<>();

    private FragmentTransaction fragmentTransaction;
    private BroadcastReceiver receiver, oReceiver, dReceiver, odReceiver;
    private ProductAdapter adapter;
    private Cart cart;
    private User user;
    private Order order;

    private boolean secondFragment = false;
    private int sortIndex = 0;
    private RelativeLayout sortingLayout;
    private EditText search;
    private ImageButton clearSearch;
    private RecyclerView listProduct;
    private SwipeRefreshLayout swipe;
    private FrameLayout loading;

    private  int[] sortButton =
            {R.id.sorting_name_az, R.id.sorting_name_za, R.id.sorting_new, R.id.sorting_old,
                    R.id.sorting_most_expensive, R.id.sorting_cheapest};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        app().main = this;
        app().prefs = new PreferencesHelper();
        app().permission = new PermissionHelper(this);
        app().account = GoogleSignIn.getLastSignedInAccount(this);
        setView();
    }

    private void setView() {
        search = findViewById(R.id.search);
        sortingLayout = findViewById(R.id.sorting_layout);
        clearSearch = findViewById(R.id.search_clear);
        swipe = findViewById(R.id.main_swipe);
        loading = findViewById(R.id.loading);

        listProduct = findViewById(R.id.product_list);
        listProduct.addItemDecoration(new GridDecoration(2, dpToPx(), true));
        listProduct.setItemAnimator(new DefaultItemAnimator());
        listProduct.setLayoutManager(new GridLayoutManager(this, 2));
        listProduct.addOnItemTouchListener(
                new RecyclerItemClickListener(this, listProduct,this));


        setOnClick(this,
                find(R.id.btn_home), find(R.id.btn_transaction), find(R.id.btn_cart), find(R.id.btn_user),
                find(R.id.btn_sort), find(R.id.search_clear), find(R.id.sorting_new), find(R.id.sorting_old),
                find(R.id.sorting_name_az), find(R.id.sorting_name_za), find(R.id.sorting_layout),
                find(R.id.sorting_most_expensive), find(R.id.sorting_cheapest), find(R.id.sort_close));

        search.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                String input = search.getText().toString();
                return !input.isEmpty();
            }
            return false;
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getData();
                clearSearch.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        swipe.setOnRefreshListener(this::getData);
    }

    private View find(int id) {
        return findViewById(id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home:
                loadFragment(Tag.HOME);
                break;
            case R.id.btn_cart:
            case R.id.btn_transaction:
                if (accountValid())
                    loadFragment(v.getId() == R.id.btn_cart ? Tag.CART : Tag.ORDER);
                else {
                    loadFragment(Tag.USER);
                    showInfo(getResources().getString(R.string.info_need_login));
                }
                break;
            case R.id.btn_user:
                loadFragment(Tag.USER);
                break;
            case R.id.search_clear:
                search.setText("");
                break;
            case R.id.btn_sort:
            case R.id.sort_close:
                sortingLayout.setVisibility(v.getId() == R.id.btn_sort ? View.VISIBLE : View.GONE);
                break;
            case R.id.sorting_name_az:
            case R.id.sorting_name_za:
            case R.id.sorting_new:
            case R.id.sorting_old:
            case R.id.sorting_most_expensive:
            case R.id.sorting_cheapest:
                sort(v.getId());
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        app().model = data.get(position);
        startActivity(new Intent(this, DetailActivity.class));
    }

    @Override
    public void onLongItemClick(View view, int position) { }

    public void loadFragment(Tag tag) {
        if (tag == Tag.HOME && tag == mTag)
            return;

        mTag = tag;
        if (getSupportFragmentManager().findFragmentByTag(tag.toString()) != null && tag != Tag.HOME)
            mTag = Tag.HOME;

        mHandler.post(() -> {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
            int attrId = secondFragment ? R.id.fragment_map : R.id.fragment_container;
            if (mTag != Tag.HOME) {
                fragmentTransaction.replace(attrId, getFragment(mTag), mTag.toString());
                fragmentTransaction.commitAllowingStateLoss();
            } else if (getSupportFragmentManager().findFragmentById(attrId) != null) {
                if (secondFragment) {
                    mTag = user != null ? Tag.USER : Tag.ORDER;
                } else {
                    user = null;
                    cart = null;
                    order = null;
                }
                secondFragment = false;
                fragmentTransaction.
                        remove(Objects.requireNonNull(
                                getSupportFragmentManager().findFragmentById(attrId))).commit();
            }
        });
    }

    private Fragment getFragment(Tag mTag) {
        switch (mTag) {
            case USER:
                return user = new User();
            case CART:
                return cart = new Cart();
            case ORDER:
                return order = new Order();
            case ORDER_DETAIL:
                return new OrderDetail();
            default:
                return new Map();
        }
    }

    public void setCoordinate(String coordinate) {
        app().prefs.setCoordinate(coordinate);
        if (user != null)
            user.refreshMarker();
    }

    public void openMap() {
        secondFragment = true;
        loadFragment(Tag.MAP);
    }

    public void openOrderDetail() {
        secondFragment = true;
        loadFragment(Tag.ORDER_DETAIL);
    }

    private void sort(int index) {
        sortingLayout.setVisibility(View.GONE);
        setSortButtonState(index);
        getData();
    }


    private void setSortButtonState(int viewId) {
        for (int i = 0; i < sortButton.length; i++) {
            find(sortButton[i]).setBackgroundColor(
                    getResources().getColor(viewId == sortButton[i] ? android.R.color.darker_gray : android.R.color.transparent));
            sortIndex = viewId == sortButton[i] ? i : sortIndex;
        }
    }

    private void refreshList() {
        if (adapter == null) {
            adapter = new ProductAdapter(this, data);
            listProduct.setAdapter(adapter);
        } else
            adapter.update(data);
    }

    private void getData() {
        MyNetwork.get(MyNetwork.Tag.PRODUCT, search.getText().toString(), sortIndex);
    }

    public void showInfo(String info) {
        Snackbar snackBar = Snackbar.make(findViewById(android.R.id.content), info, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", v -> snackBar.dismiss());
        snackBar.show();
    }

    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (receiver == null) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    assert bundle != null;
                    String result = bundle.getString(MyNetwork.Key.data.name());
                    data = MyNetwork.createProductModel(result);
                    refreshList();
                    if (swipe.isRefreshing())
                        swipe.setRefreshing(false);
                }
            };
            IntentFilter intentFilter = new IntentFilter(MyNetwork.Tag.PRODUCT.name());
            registerReceiver(receiver, intentFilter);
            getData();
        }

        if (oReceiver == null) {
            oReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    assert bundle != null;
                    String result = bundle.getString(MyNetwork.Key.data.name());
                    int id = getStatus(result, MyNetwork.Key.id);
                    if (cart != null && getStatus(result) == 1 && id > 0)
                        cart.update(id);
                    else {
                        hideLoading();
                        showInfo(getResources().getString(R.string.info_connection_issue));
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter(MyNetwork.Tag.ORDER.name());
            registerReceiver(oReceiver, intentFilter);
        }

        if (dReceiver == null) {
            dReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    assert bundle != null;
                    String result = bundle.getString(MyNetwork.Key.data.name());
                    if (getStatus(result) != 1) {
                        hideLoading();
                        app().cartData.clear();
                        showInfo(getResources().getString(R.string.info_connection_issue));
                    }
                    if (app().cartData.size() > 0) {
                        app().cartData.remove(0);
                        if (app().cartData.size() == 0) {
                            hideLoading();
                            showInfo(getResources().getString(R.string.title_will_be_delivered));
                            if (cart != null)
                                cart.updatePrice();
                        }
                        MyNetwork.get(MyNetwork.Tag.GET_ORDER);
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter(MyNetwork.Tag.ORDER_DETAIL.name());
            registerReceiver(dReceiver, intentFilter);
        }

        if (odReceiver == null) {
            odReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    Log.e("Test222", "odReceiver");
                    assert bundle != null;
                    String result = bundle.getString(MyNetwork.Key.data.name());
                    if (getStatus(result) == 1) {
                        app().orderData = MyNetwork.createOrderModel(result);
                        if (order != null)
                            order.updateList();
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter(MyNetwork.Tag.GET_ORDER.name());
            registerReceiver(odReceiver, intentFilter);
            if (!getEmail().isEmpty())
                MyNetwork.get(MyNetwork.Tag.GET_ORDER);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        app().permission.onRequestCallBack(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (mTag != Tag.HOME)
            loadFragment(Tag.HOME);
        else if (!search.getText().toString().isEmpty()) {
            search.setText("");
        } else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null)
            unregisterReceiver(receiver);
        if (oReceiver != null)
            unregisterReceiver(oReceiver);
        if (dReceiver != null)
            unregisterReceiver(dReceiver);
        if (odReceiver != null)
            unregisterReceiver(odReceiver);
    }
}
