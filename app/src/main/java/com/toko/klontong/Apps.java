package com.toko.klontong;

import android.app.Application;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.toko.klontong.utils.model.Model;
import com.toko.klontong.utils.PermissionHelper;
import com.toko.klontong.utils.PreferencesHelper;
import com.toko.klontong.utils.model.OrderModel;

import java.util.ArrayList;

public class Apps extends Application {

    private static Apps instance;
    public GoogleSignInAccount account;
    public MainActivity main;

    public PermissionHelper permission;
    public PreferencesHelper prefs;

    public ArrayList<Model> cartData = new ArrayList<>();
    public ArrayList<OrderModel> orderData = new ArrayList<>();
    public int orderDetail = 0;
    public Model model;

    public static Apps app() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
