package com.toko.klontong.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.toko.klontong.R;
import com.toko.klontong.utils.model.OrderModel;

import java.util.ArrayList;

import static com.toko.klontong.utils.Utils.getCurrency;


public class OrderAdapter extends
        RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private ArrayList<OrderModel> mData;
    private LayoutInflater mInflater;

    public OrderAdapter(Context context, ArrayList<OrderModel> data) {
        mInflater = LayoutInflater.from(context);
        mData = data;
    }

    public void update(ArrayList<OrderModel> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(
                R.layout.main_order_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        OrderModel model = mData.get(position);
        holder.date.setText(model.date);
        holder.price.setText(getCurrency(model.price));
        holder.status.setText(model.status);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView date, status, price;

        private ViewHolder(View v) {
            super(v);
            date  = v.findViewById(R.id.order_date);
            status = v.findViewById(R.id.order_status);
            price  = v.findViewById(R.id.order_price);
        }
    }

}
