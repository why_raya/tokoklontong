package com.toko.klontong.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.toko.klontong.R;
import com.toko.klontong.utils.model.Model;

import java.util.ArrayList;

import static com.toko.klontong.utils.Utils.getCurrency;


public class CartAdapter extends
        RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private ArrayList<Model> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private Listener mListener;

    public CartAdapter(Context context, ArrayList<Model> data, @NonNull Listener listener) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        mContext = context;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(
                R.layout.main_cart_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Model model = mData.get(position);
        holder.title.setText(model.getName());
        holder.price.setText(getCurrency(model.getFixedPrice()));
        holder.count.setText(String.valueOf(model.getCount()));

        holder.add.setOnClickListener(v -> mListener.onButtonCount(1, position));

        holder.min.setOnClickListener(v -> mListener.onButtonCount(-1, position));

        holder.remove.setOnClickListener(v -> mListener.onRemoved(position));

        Glide.with(mContext).load(model.getImage()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title, price;
        private EditText count;
        private ImageView thumbnail;
        private Button remove;
        private ImageButton add, min;

        private ViewHolder(View v) {
            super(v);
            title  = v.findViewById(R.id.product_title);
            price  = v.findViewById(R.id.product_price);
            add    = v.findViewById(R.id.product_count_add);
            min    = v.findViewById(R.id.product_count_min);
            count  = v.findViewById(R.id.product_count);
            remove = v.findViewById(R.id.product_remove);
            thumbnail = v.findViewById(R.id.product_image);
        }
    }

    public interface Listener {

        void onButtonCount(int value, int pos);

        void onRemoved(int pos);
    }
}
