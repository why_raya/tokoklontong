package com.toko.klontong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.toko.klontong.R;
import com.toko.klontong.utils.model.Model;

import java.util.ArrayList;

import static com.toko.klontong.utils.Utils.getCurrency;

public class ProductAdapter extends
        RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private ArrayList<Model> mData;
    private LayoutInflater mInflater;
    private Context mContext;

    public ProductAdapter(Context context, ArrayList<Model> data) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        mContext = context;
    }

    public void update(ArrayList<Model> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(
                R.layout.main_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder vh, int position) {
        Model m = mData.get(position);

        vh.title.setText(m.getName());
        vh.price.setText(getCurrency(m.getPrice()));
        vh.discount.setVisibility(m.getDiscount() > 0 ? View.VISIBLE : View.GONE);
        vh.discountPrice.setVisibility(m.getDiscount() > 0 ? View.VISIBLE : View.GONE);
        if (m.getDiscount() > 0) {
            vh.discount.setText(mContext.getResources().getString(
                    R.string.title_discount) + " " + m.getDiscount() + "%");
            vh.discountPrice.setText(getCurrency(m.getFixedPrice()));
            vh.price.setPaintFlags(vh.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else
            vh.price.setPaintFlags(vh.price.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));

        Glide.with(mContext).load(m.getImage()).into(vh.thumbnail);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title, price, discount, discountPrice;
        private ImageView thumbnail;

        private ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.product_title);
            price = v.findViewById(R.id.product_price);
            discount = v.findViewById(R.id.product_discount);
            discountPrice = v.findViewById(R.id.product_price_discount);
            thumbnail = v.findViewById(R.id.product_image);
        }
    }
}
