package com.toko.klontong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.toko.klontong.R;
import com.toko.klontong.utils.model.OrderModel;

import java.util.ArrayList;

import static com.toko.klontong.utils.Utils.getCurrency;


public class OrderDetailAdapter extends
        RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {

    private ArrayList<OrderModel> mData;
    private LayoutInflater mInflater;

    public OrderDetailAdapter(Context context, ArrayList<OrderModel> data) {
        mInflater = LayoutInflater.from(context);
        mData = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(
                R.layout.main_order_detail_item, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        OrderModel model = mData.get(position);
        int total = model.price * model.quantity;
        holder.name.setText(model.name);
        holder.price.setText(getCurrency(model.price));
        holder.total.setText(getCurrency(total));
        holder.quantity.setText("x" + model.quantity);
        holder.discountLayout.setVisibility(model.discount > 0 ? View.VISIBLE : View.GONE);
        if (model.discount > 0) {
            holder.discount.setText("Diskon " + model.discount + "%");
            holder.discountPrice.setText("- " + getCurrency(total * model.discount / 100));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name, quantity, price, total, discount, discountPrice;
        private LinearLayout discountLayout;

        private ViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.product_name);
            quantity = v.findViewById(R.id.product_quantity);
            price  = v.findViewById(R.id.product_price);
            total  = v.findViewById(R.id.product_price_total);
            discount  = v.findViewById(R.id.product_discount);
            discountPrice = v.findViewById(R.id.product_price_discount);
            discountLayout = v.findViewById(R.id.discount_layout);
        }
    }

}
