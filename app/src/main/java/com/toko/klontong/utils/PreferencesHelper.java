package com.toko.klontong.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.Utils.getEmail;

/**
 * Created by raya on 11/07/17.
 */

public class PreferencesHelper {

    private SharedPreferences prefs;

    public PreferencesHelper() {
        prefs = PreferenceManager.getDefaultSharedPreferences(app());
    }

    public void setUsername(String value) {
        prefs.edit().putString(key(MyNetwork.Key.name), value).apply();
    }

    public String getUsername() {
        return prefs.getString(key(MyNetwork.Key.name),"");
    }

    public void setPhone(String value) {
        prefs.edit().putString(key(MyNetwork.Key.phone), value).apply();
    }

    public String getPhone() {
        return prefs.getString(key(MyNetwork.Key.phone),"");
    }

    public void setAddress(String value) {
        prefs.edit().putString(key(MyNetwork.Key.address), value).apply();
    }

    public String getAddress() {
        return prefs.getString(key(MyNetwork.Key.address),"");
    }

    public void setCoordinate(String value) {
        prefs.edit().putString(key(MyNetwork.Key.coordinate), value).apply();
    }

    public String getCoordinate() {
        return prefs.getString(key(MyNetwork.Key.coordinate),"");
    }

    private String key(MyNetwork.Key key) {
        return getEmail() + key.name();
    }
}
