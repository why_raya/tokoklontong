package com.toko.klontong.utils.model;

public class Model {

    private int id = 0;
    private String image = "";
    private String name = "";
    private String desc = "";
    private int count = 1;
    private float price;
    private float fixedPrice;
    private int discount = 0;
    private int stock = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getFixedPrice() {
        return fixedPrice;
    }

    public void setFixedPrice(float fixedPrice) {
        this.fixedPrice = fixedPrice;
    }
}
