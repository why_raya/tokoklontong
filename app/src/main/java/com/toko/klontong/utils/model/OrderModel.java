package com.toko.klontong.utils.model;

public class OrderModel {

    public String name = "";
    public String address = "";
    public String phone = "";
    public String status = "";
    public String desc = "";
    public String date = "";

    public int price = 0;
    public int discount = 0;
    public int quantity = 0;
}
