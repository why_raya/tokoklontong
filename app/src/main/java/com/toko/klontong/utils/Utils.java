package com.toko.klontong.utils;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

import static com.toko.klontong.Apps.app;

public class Utils {

    public static void setOnClick(@NotNull View.OnClickListener listener, View... views) {
        for (View v : views)
            v.setOnClickListener(listener);
    }

    public static void setKeyNull(EditText... editTexts) {
        for (EditText et : editTexts)
            et.setKeyListener(null);
    }

    public static void hideKeyboard(Context ctx, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getCurrency(float price) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
        String currency = format.format(price);
        return currency.replace("Rp", "Rp ");
    }

    public static int dpToPx() { // Converting dp to pixel
        return Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 2, app().getResources().getDisplayMetrics()));
    }

    public static boolean accountValid() {
        return !getEmail().isEmpty();
    }

    public static String getEmail() {
        return app().account != null ? app().account.getEmail() : "";
    }
}
