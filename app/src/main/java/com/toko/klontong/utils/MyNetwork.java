package com.toko.klontong.utils;

import android.content.Intent;

import com.toko.klontong.utils.model.Model;
import com.toko.klontong.utils.model.OrderModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.Utils.getEmail;

public class MyNetwork {

    public enum Tag {
        ORDER_DETAIL, ORDER, PRODUCT, GET_ORDER
    }

    public enum Key {
        id, id_orders, id_product, name, email, phone, model, status, data, product, type,
        desc, view, address, date, coordinate,
        quantity, sort, image, lat, lon, discount, price, stock
    }

    private static final String endPoint = "http://skripsiku.xyz/sembakone/";
    private static final String apiURL = endPoint + "api/api.php";

    public static void get(final Tag tag) {
        get(tag, "", 0);
    }

    public static void get(final Tag tag, String search, int sortIndex) {
        run(new FormBody.Builder()
                        .add(Key.type.name(), tag.name())
                        .add(Key.product.name(), search)
                        .add(Key.email.name(), getEmail())
                        .add(Key.sort.name(), String.valueOf(sortIndex))
                        .build(), tag);
    }

    public static void set(Tag tag, FormBody body) {
        run(body, tag);
    }

    private static void run(final RequestBody formBody, final Tag tag) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(apiURL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                broadcast(-99,"", tag);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                try {
                    String result = Objects.requireNonNull(response.body()).string();
                    broadcast(response.code(), result, tag);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void broadcast(int statusCode, String result, Tag tag) {
        Intent intent = new Intent(tag.name());
        intent.putExtra(Key.status.name(), statusCode);
        intent.putExtra(Key.data.name(), result);
        app().sendBroadcast(intent);
    }

    public static int getStatus(String result) {
        return getStatus(result, Key.status);
    }

    public static int getStatus(String result, Key _key) {
        try {
            return new JSONObject(result).getInt(_key.name());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -99;
    }

    public static ArrayList<Model> createProductModel(String result) {
        ArrayList<Model> data = new ArrayList<>();
        if (getStatus(result) == 1) {
            try {
                JSONArray arr = new JSONObject(result).getJSONArray(Key.data.name());
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);

                    Model m = new Model();
                    m.setId(obj.getInt(Key.id_product.name()));
                    m.setName(obj.getString(Key.name.name()));
                    m.setPrice(obj.getInt(Key.price.name()));
                    m.setDesc(obj.getString(Key.desc.name()));
                    m.setImage(endPoint + obj.getString(Key.image.name()));
                    m.setDiscount(obj.getInt(Key.discount.name()));
                    m.setStock(obj.getInt(Key.stock.name()));
                    m.setFixedPrice((m.getPrice() - (m.getPrice() * m.getDiscount() / 100)));
                    if (m.getStock() > 0)
                        data.add(m);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static FormBody getOrderBody() {
        String[] coordinates = app().prefs.getCoordinate().split(", ");
        return new FormBody.Builder()
                .add(Key.type.name(), Tag.ORDER.name())
                .add(Key.name.name(), app().prefs.getUsername())
                .add(Key.phone.name(), app().prefs.getPhone())
                .add(Key.address.name(), app().prefs.getAddress())
                .add(Key.lat.name(), coordinates[0])
                .add(Key.lon.name(), coordinates[1])
                .add(Key.email.name(), getEmail())
                .build();
    }

    public static FormBody getOrderDetailBody(int id, Model m) {
        return new FormBody.Builder()
                .add(Key.type.name(), Tag.ORDER_DETAIL.name())
                .add(Key.id_orders.name(), String.valueOf(id))
                .add(Key.id_product.name(), String.valueOf(m.getId()))
                .add(Key.quantity.name(), String.valueOf(m.getCount()))
                .add(Key.price.name(), String.valueOf(m.getPrice()))
                .add(Key.discount.name(), String.valueOf(m.getDiscount()))
                .build();
    }


    public static ArrayList<OrderModel> createOrderModel(String result) {
        ArrayList<OrderModel> data = new ArrayList<>();
        try {
            JSONArray arr = new JSONObject(result).getJSONArray(Key.data.name());
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);

                OrderModel m = new OrderModel();
                m.date = obj.getString(Key.date.name());
                m.address = obj.getString(Key.address.name());
                m.phone = obj.getString(Key.phone.name());
                m.quantity = obj.getInt(Key.quantity.name());
                m.status = obj.getString(Key.status.name());
                m.desc = obj.getString(Key.desc.name());
                m.price = obj.getInt(Key.price.name());
                data.add(m);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static ArrayList<OrderModel> createOrderDetailModel(String result) {
        ArrayList<OrderModel> data = new ArrayList<>();
        if (getStatus(result) == 1) {
            try {
                JSONArray arr = new JSONObject(result).getJSONArray(Key.data.name());
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);

                    OrderModel m = new OrderModel();
                    m.name = obj.getString(Key.name.name());
                    m.discount = obj.getInt(Key.discount.name());
                    m.price = obj.getInt(Key.price.name());
                    m.quantity = obj.getInt(Key.quantity.name());
                    data.add(m);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return data;
    }
}