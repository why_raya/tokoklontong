package com.toko.klontong.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

public class MyTextWatcher implements TextWatcher {

    private View view;
    private Listener listener;

    public MyTextWatcher(View view, Listener listener) {
        this.view = view;
        this.listener = listener;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (listener != null)
            listener.onTextChanged(view.getId(), charSequence);
    }

    public void afterTextChanged(Editable editable) { }

    public interface Listener {
        void onTextChanged(int id, CharSequence charSequence);
    }
}