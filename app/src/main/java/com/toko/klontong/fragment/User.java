package com.toko.klontong.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.toko.klontong.R;
import com.toko.klontong.utils.MyNetwork;
import com.toko.klontong.utils.MyTextWatcher;

import java.util.Objects;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.Utils.accountValid;
import static com.toko.klontong.utils.Utils.getEmail;
import static com.toko.klontong.utils.Utils.setOnClick;

public class User extends Fragment implements OnMapReadyCallback, View.OnClickListener, MyTextWatcher.Listener {

    private TextView user, email, address, phone;
    private LinearLayout profile;
    private SignInButton login;
    private ImageView img;

    private GoogleSignInClient client;

    private GoogleMap mMap;
    private Marker marker;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user, container, false);

        login = view.findViewById(R.id.sign_in_button);
        img  = view.findViewById(R.id.user_img);
        user  = view.findViewById(R.id.user_name);
        email = view.findViewById(R.id.user_email);
        address = view.findViewById(R.id.user_address);
        phone = view.findViewById(R.id.user_phone);
        profile  = view.findViewById(R.id.user_profile);
        TextView signInStatus = (TextView) login.getChildAt(0);
        signInStatus.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        client = GoogleSignIn.getClient(Objects.requireNonNull(getActivity()),
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build());

        setOnClick(this, view, view.findViewById(R.id.show_map),
                view.findViewById(R.id.sign_in_button), view.findViewById(R.id.sign_out_button));
        updateUI();

        return view;
    }

    private void updateUI() {
        profile.setVisibility(accountValid() ? View.VISIBLE : View.GONE);
        login.setVisibility(accountValid() ? View.GONE : View.VISIBLE);
        if (accountValid()) {
            user.setText(app().account.getDisplayName());
            email.setText(getEmail());
            address.setText(app().prefs.getAddress());
            phone.setText(app().prefs.getPhone());
            address.addTextChangedListener(new MyTextWatcher(address, this));
            phone.addTextChangedListener(new MyTextWatcher(phone, this));
            Glide.with(Objects.requireNonNull(getActivity())).load(app().account.getPhotoUrl()).into(img);
            try {
                FragmentManager fm = getChildFragmentManager();
                SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentByTag("mapFragment");
                if (mapFragment == null) {
                    mapFragment = new SupportMapFragment();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.add(R.id.mapContainer, mapFragment, "mapFragment");
                    ft.commit();
                    fm.executePendingTransactions();
                }
                Objects.requireNonNull(mapFragment).getMapAsync(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        app().main.hideLoading();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                app().main.showLoading();
                startActivityForResult(client.getSignInIntent(), 123);
                break;
            case R.id.sign_out_button:
                app().main.showLoading();
                signOut();
                break;
            case R.id.show_map:
                app().main.openMap();
                break;
        }
    }

    @Override
    public void onTextChanged(int id, CharSequence charSequence) {
        switch(id) {
            case R.id.user_address:
                app().prefs.setAddress(charSequence.toString());
                break;
            case R.id.user_phone:
                app().prefs.setPhone(charSequence.toString());
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        refreshMarker();
    }


    public void refreshMarker() {
        if (marker != null)
            marker.remove();

        LatLng latLng = new LatLng(-6.990172, 110.422943);
        if (!app().prefs.getCoordinate().isEmpty()) {
            String[] coordinates = app().prefs.getCoordinate().split(", ");
            latLng = new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
            marker = mMap.addMarker(new MarkerOptions().position(latLng));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 123 && resultCode == -1) {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                app().account = task.getResult(ApiException.class);
                assert app().account != null;
                app().prefs.setUsername(app().account.getDisplayName());
                MyNetwork.get(MyNetwork.Tag.GET_ORDER);
                updateUI();
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
        app().main.hideLoading();
    }

    private void signOut() {
        client.signOut()
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    app().account = null;
                    app().cartData.clear();
                    app().orderData.clear();
                    updateUI();
                });
    }
}