package com.toko.klontong.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.toko.klontong.R;
import com.toko.klontong.adapter.CartAdapter;
import com.toko.klontong.utils.model.Model;
import com.toko.klontong.utils.MyNetwork;

import java.util.Objects;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.MyNetwork.getOrderBody;
import static com.toko.klontong.utils.MyNetwork.getOrderDetailBody;
import static com.toko.klontong.utils.Utils.getCurrency;

public class Cart extends Fragment implements CartAdapter.Listener {

    private TextView totalPrice;
    private CartAdapter adapter;
    private LinearLayout emptyCart;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_cart, container, false);
        totalPrice = view.findViewById(R.id.product_price_total);
        emptyCart = view.findViewById(R.id.empty_cart);
        RecyclerView lv = view.findViewById(R.id.product_list);
        lv.setItemAnimator(new DefaultItemAnimator());
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new CartAdapter(getActivity(), app().cartData, this);
        lv.setAdapter(adapter);
        updatePrice();
        view.findViewById(R.id.product_payment).setOnClickListener(v -> order());

        return view;
    }

    private void order() {
        if (app().cartData.size() == 0)
            return;

        if (app().prefs.getCoordinate().isEmpty() ||
                app().prefs.getAddress().isEmpty() || app().prefs.getAddress().length() < 5 ||
                app().prefs.getPhone().isEmpty() || app().prefs.getPhone().length() < 10) {
            app().main.showInfo(Objects.requireNonNull(getActivity()).getString(R.string.info_need_to_complete_bio));
            return;
        }

        app().main.showLoading();
        MyNetwork.set(MyNetwork.Tag.ORDER, getOrderBody());
    }

    public void update(int id) {
        for (Model m : app().cartData)
            MyNetwork.set(MyNetwork.Tag.ORDER_DETAIL, getOrderDetailBody(id, m));
    }

    public synchronized void updatePrice() {
        adapter.notifyDataSetChanged();
        float price = 0f;
        for (Model m : app().cartData)
            price += m.getFixedPrice() * (float) m.getCount();
        totalPrice.setText(getCurrency(price));
        emptyCart.setVisibility(app().cartData.size() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onButtonCount(int value, int pos) {
        int v = app().cartData.get(pos).getCount() + value;
        app().cartData.get(pos).setCount(v < 1 ? 1 : v);
        updatePrice();
    }

    @Override
    public void onRemoved(int pos) {
        app().cartData.remove(pos);
        updatePrice();
    }
}
