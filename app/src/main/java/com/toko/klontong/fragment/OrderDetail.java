package com.toko.klontong.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.toko.klontong.R;
import com.toko.klontong.adapter.OrderDetailAdapter;
import com.toko.klontong.utils.model.OrderModel;

import static com.toko.klontong.Apps.app;
import static com.toko.klontong.utils.MyNetwork.createOrderDetailModel;
import static com.toko.klontong.utils.Utils.getCurrency;

public class OrderDetail extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_order, container, false);

        OrderModel m = app().orderData.get(app().orderDetail);

        RecyclerView lv = view.findViewById(R.id.order_list);
        lv.setItemAnimator(new DefaultItemAnimator());
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
        lv.setAdapter(new OrderDetailAdapter(getActivity(),
                createOrderDetailModel(m.desc)));

        TextView total = view.findViewById(R.id.product_price_total);
        total.setText(getCurrency(m.price));

        view.findViewById(R.id.info).setVisibility(View.VISIBLE);
        view.setOnClickListener(v -> { });
        return view;
    }


}
