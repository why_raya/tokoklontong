package com.toko.klontong.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.toko.klontong.R;
import com.toko.klontong.adapter.OrderAdapter;
import com.toko.klontong.utils.RecyclerItemClickListener;

import static com.toko.klontong.Apps.app;

public class Order extends Fragment implements RecyclerItemClickListener.OnItemClickListener {

    private OrderAdapter adapter;
    private LinearLayout emptyCart;
    private RecyclerView lv;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_order, container, false);

        emptyCart = view.findViewById(R.id.empty_order);
        lv = view.findViewById(R.id.order_list);
        lv.setItemAnimator(new DefaultItemAnimator());
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));
        lv.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), lv,this));
        updateList();
        view.setOnClickListener(v -> { });
        return view;
    }

    public void updateList() {
        if (adapter == null) {
            adapter = new OrderAdapter(getActivity(), app().orderData);
            lv.setAdapter(adapter);
        } else
            adapter.update(app().orderData);
        emptyCart.setVisibility(app().orderData.size() == 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onItemClick(View view, int position) {
        app().orderDetail = position;
        app().main.openOrderDetail();
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }
}
