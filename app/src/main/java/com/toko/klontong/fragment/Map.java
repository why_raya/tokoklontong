package com.toko.klontong.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.toko.klontong.R;
import com.toko.klontong.utils.PermissionHelper;
import com.toko.klontong.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.toko.klontong.Apps.app;

public class Map extends Fragment implements View.OnClickListener, OnMapReadyCallback,
        PermissionHelper.PermissionListener {

    private GoogleMap mMap;
    private String coordinate = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.map, container, false);

        app().permission.setPermissionListener(this);

//        coordinate = app().address.getCoordinate();
        try {
            FragmentManager fm = getChildFragmentManager();
            SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentByTag("mapFragment");
            if (mapFragment == null) {
                mapFragment = new SupportMapFragment();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.map, mapFragment, "mapFragment");
                ft.commit();
                fm.executePendingTransactions();
            }
            Objects.requireNonNull(mapFragment).getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.setOnClick(this, v, v.findViewById(R.id.map_cancel), v.findViewById(R.id.map_ok));
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.map_cancel:
                Objects.requireNonNull(getActivity()).onBackPressed();
                break;
            case R.id.map_ok:
                 app().main.setCoordinate(mMap.getCameraPosition().target.latitude + ", " + mMap.getCameraPosition().target.longitude);
                Objects.requireNonNull(getActivity()).onBackPressed();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-6.990172, 110.422943);
        if (!coordinate.isEmpty()) {
            String[] coordinates = coordinate.split(", ");
            latLng = new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
        setLocation();
    }

    private void setLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Objects.requireNonNull(getActivity()).checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                List<String> listPermissions = new ArrayList<>();
                listPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                listPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                app().permission.checkAndRequestPermissions(listPermissions);
            } else
                mMap.setMyLocationEnabled(true);
        } else
            mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onPermissionCheckDone() {
        setLocation();
    }
}
